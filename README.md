# FreeRADIUS konfiguracija za eduroam uslugu

Ovaj repozitorij nastao je inicijalnom kopijom /etc/raddb konfiguracijskog direktorija FreeRADIUS paketa za Centos 7 u Git repozitorij (master grana).

**[SP grana](https://gitlab.com/dpenezic/eduroam-sp-dei-freeradius/tree/SP)** sadrzi konfiguraciju i upute za pruzatelje usluge eduroam.

Inicijalno konfiguracije i upute su nastale kao podloga za radionicu na DEI 2019.


Repozitorij se nastavlja ne Git repozitorij dostupan na [GEANT eduroam TtT](https://github.com/GEANT/eduroam-training-FreeRADIUS-configuration/) .

